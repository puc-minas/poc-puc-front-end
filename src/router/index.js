import HelloWorld from '@/components/HelloWorld'
import ClientsList from '@/components/Clients/List'
import ClientsView from '@/components/Clients/View'
import ClientsCreate from '@/components/Clients/Create'
import ClientsEdit from '@/components/Clients/Edit'
import Login from '@/components/Login/Form'
import ProvidersList from '@/components/Providers/List'
import ProvidersView from '@/components/Providers/View'
import ProvidersCreate from '@/components/Providers/Create'
import ProvidersEdit from '@/components/Providers/Edit'
import ProductsList from '@/components/Products/List'
import ProductsCreate from '@/components/Products/Create'

const routes = [
  {
    path: '/',
    name: 'HelloWorld',
    meta: { siderBar: false },
    component: HelloWorld
  },
  {
    path: '/clientes',
    name: 'ClientsList',
    component: ClientsList
  },
  {
    path: '/clientes/novo',
    name: 'ClientsCreate',
    component: ClientsCreate
  },
  {
    path: '/clientes/:id/editar',
    name: 'ClientsEdit',
    component: ClientsEdit
  },
  {
    path: '/clientes/:id',
    name: 'ClientsView',
    component: ClientsView
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/fornecedores',
    name: 'ProvidersList',
    component: ProvidersList
  },
  {
    path: '/fornecedores/novo',
    name: 'ProvidersCreate',
    component: ProvidersCreate
  },
  {
    path: '/fornecedores/:id/editar',
    name: 'ProvidersEdit',
    component: ProvidersEdit
  },
  {
    path: '/fornecedores/:id',
    name: 'ProvidersView',
    component: ProvidersView
  },
  {
    path: '/produtos',
    name: 'ProductsList',
    component: ProductsList
  },
  {
    path: '/produtos/novo',
    name: 'ProductsCreate',
    component: ProductsCreate
  }
]

export default routes
