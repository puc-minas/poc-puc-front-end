import Vue from 'vue'

export default {
  state: {
    clientsList: [],
    clientView: {},
    clientsListLoading: false
  },
  mutations: {
    updateClientsList (state, data) {
      state.clientsList = data
    },
    updateClientView (state, data) {
      state.clientView = data
    },
    updateClientsListLoading (state, boo) {
      console.log(boo)
      state.clientsListLoading = boo
    }
  },
  actions: {
    getClients (context) {
      context.commit('updateClientsListLoading', true)
      Vue.http.get('api/clients').then(response => {
        context.commit('updateClientsList', response.data)
        context.commit('updateClientsListLoading', false)
      })
    },
    getClient (context, id) {
      Vue.http.get('api/clients/' + id).then(response => {
        context.commit('updateClientView', response.data)
      })
    },
    createClient (context, data) {
      Vue.http.post('api/clients', data)
    },
    updateClient (context, params) {
      Vue.http.put('api/clients/' + params.id, params.data)
    },
    removeClient (context, id) {
      Vue.http.delete('api/clients/' + id)
    }
  }
}
