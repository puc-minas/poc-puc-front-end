import Vue from 'vue'

export default {
  state: {
    providersList: [],
    providerView: {},
    providersListLoading: false
  },
  mutations: {
    updateProvidersList (state, data) {
      state.providersList = data
    },
    updateProviderView (state, data) {
      state.providerView = data
    },
    updateProvidersListLoading (state, boo) {
      state.providersListLoading = boo
    }
  },
  actions: {
    getproviders (context) {
      context.commit('updateProvidersListLoading', true)
      Vue.http.get('api/providers').then(response => {
        context.commit('updateProvidersList', response.data)
        context.commit('updateProvidersListLoading', false)
      })
    },
    getProvider (context, id) {
      Vue.http.get('api/providers/' + id).then(response => {
        context.commit('updateProviderView', response.data)
      })
    },
    createProvider (context, data) {
      Vue.http.post('api/providers', data)
    },
    updateProvider (context, params) {
      Vue.http.put('api/providers/' + params.id, params.data)
    },
    removeProvider (context, id) {
      Vue.http.delete('api/providers/' + id)
    }
  }
}
