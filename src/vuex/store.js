import clients from './modules/clients'
import providers from './modules/providers'
import pagination from './modules/pagination'

export default {
  modules: {
    clients: clients,
    pagination: pagination,
    providers: providers
  }
}
